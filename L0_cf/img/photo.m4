% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   { battery(up_ l); }      
   { move up_ l*0.75 left l*0.15
   em_arrows(N,315);}
   source(up_ l,,0.3);
   line right_ l*0.5
   dot
   {
   source(down_ Here.y-Origin.y,"V");
   dot
   }
   line right l*0.4
   { 
    move right 0.035
    move up 0.02
    line down l*0.25+0.02 right l*0.15-0.035
    dot(,,1);
    move down 0.02
    resistor(down_ Here.y-Origin.y); rlabel(,R,); variable();
    dot
   }
   dot(,,1);
   move right_ l*0.3
   dot(,,1);
   move right 0.02
   line right_ l*0.4
   source(down_ Here.y-Origin.y,"$\Omega$");
   line to Origin
.PE
