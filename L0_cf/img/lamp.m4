% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   source(up_ l,V);
   resistor(right_ l); rlabel(,R,); variable();
   dot
   {
    source(down_ Here.y-Origin.y,"V");
    dot
   }
   line right_ l*0.5
   source(down_ Here.y-Origin.y,X);
   line to Origin
.PE
