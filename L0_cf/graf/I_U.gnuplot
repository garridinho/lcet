reset

#epslatex
set terminal epslatex size 18cm,14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'I_U.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#800000' lt 1 lw 2 pt 6 ps 2 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#ff0000' lt 1 lw 2 pt 6 ps 2
set style line 3 lc rgb '#0000ff' lt 1 lw 2 pt 6 ps 2
set style line 4 lc rgb '#ffa500' lt 1 lw 2 pt 6 ps 2
set style line 5 lc rgb '#9400d3' lt 1 lw 2 pt 6 ps 2
unset key

#Title
#set title "Caracter\\\'{\\i}stica El\\\'{e}trica"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12 #insere grelha

#set mxtics 2
#set mytics 10

set yrange [0:9]
set ytics 1,1,9 #1st,step,last
#set logscale y
#set yrange [1e7:1e22]

#Legenda
set key top right title 'Inclina\c{c}\~{a}o' box 5
#set key at 9.9,3.2 title 'Inclina\c{c}\~{a}o' box 5

#axis labels
set xlabel 'U/ V'
set ylabel 'I/ mA' #font ",20"
set tics scale 2 

set bars 2 #espessura das barras de erro

#fits
I_0(x)=a_0-b_0*exp(c_0*x) 
I_15(x)=a_15-b_15*exp(c_15*x)
I_30(x)=a_30-b_30*exp(c_30*x)
I_45(x)=a_45-b_45*exp(c_45*x)
I_60(x)=a_60-b_60*exp(c_60*x)
a_0=8; b_0=0.1; c_0=1.4;
a_15=8; b_15=0.1; c_15=1.4; 
a_30=8; b_30=0.1; c_30=1.4;
a_45=8; b_45=0.1; c_45=1.4;
a_60=8; b_60=0.1; c_60=1.4;
fit I_0(x) 'I_U_0.txt' via a_0,b_0,c_0
fit I_15(x) 'I_U_15.txt' via a_15,b_15,c_15
fit I_30(x) 'I_U_30.txt' via a_30,b_30,c_30
fit I_45(x) 'I_U_45.txt' via a_45,b_45,c_45
fit I_60(x) 'I_U_60.txt' via a_60,b_60,c_60

plot 'I_U_0.txt'   u 1:2:3:4 title "$0^o$"  w xyerrorbars ls 1, \
     I_0(x) title "fit $0^o$" ls 1, \
     'I_U_15.txt'  u 1:2:3:4 title "$15^o$" w xyerrorbars ls 2, \
     I_15(x) title "fit $15^o$" ls 2, \
     'I_U_30.txt'  u 1:2:3:4 title "$30^o$" w xyerrorbars ls 3, \
     I_30(x) title "fit $30^o$" ls 3, \
     'I_U_45.txt'  u 1:2:3:4 title "$45^o$" w xyerrorbars ls 4, \
     I_45(x) title "fit $45^o$" ls 4, \
     'I_U_60.txt'  u 1:2:3:4 title "$60^o$" w xyerrorbars ls 5, \
     I_60(x) title "fit $60^o$" ls 5
