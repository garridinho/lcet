reset

# epslatex
set terminal epslatex size 18cm,14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'U_0_cos.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#0000ff' lt 1 lw 2 pt 6 ps 2 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#000000' lt 1 lw 2 pt 6 ps 2


#Title
#set title "Linearidade de grandezas em fun\\\c{c}\\\{a}o do \\\^{a}ngulo de inclina\\\c{c}\\\{a}o"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
#<<<<<<< HEAD
#set grid back ls 12 pi -5
#=======
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12
#>>>>>>> a437dfe64b3b18657d3a3fb2ff84f1c5cc3d3f2b

#set mxtics 2
#set mytics 10

#Legenda
set key top right title 'Legenda' box 5
#set key top right title "" box 5

#axis labels
set xlabel 'cos($\alpha$)'


set tics scale 2

set bars 2 #espessura das barras de erro

#fits
max(x)=a+b/(x+c)
#a=-1.1;b=6100;c=141;
fit max(x) 'U_0_cos.txt' u 3:5 via a,b,c


set xrange [0.1:1.4]
set yrange [2:9]
plot 'U_0_cos.txt' u 3:5:4:6 title "$I(U=0)$" w xyerrorbars ls 2,\
	 max(x) title "Grandeza linear" ls 1

