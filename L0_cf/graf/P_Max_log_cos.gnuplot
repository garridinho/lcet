reset

# epslatex
set terminal epslatex size 18cm,14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'P_Max_log_cos.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#0000ff' lt 1 lw 2 pt 6 ps 2 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#000000' lt 1 lw 2 pt 6 ps 2


#para quando aprender a utilizar uma coluna do ficheiro para selecionar o tipo de ponto
set style line 9 lc rgb '#ff0000' lt 1 lw 2 pt 11 ps 1
#set style line 10 lc rgb '#0000ff' lt 1 lw 2 pt 11 ps 1
#set style line 11 lc rgb '#ffa500' lt 1 lw 2 pt 11 ps 1
set style line 12 lc rgb '#9400d3' lt 1 lw 2 pt 11 ps 1
set style line 13 lc rgb '#00ffa5' lt 1 lw 2 pt 11 ps 1
set style line 14 lc rgb '#800000' lt 1 lw 2 pt 11 ps 1
unset key

#Title
set title "Linearidade de grandezas em fun\\\c{c}\\\~{a}o do \\\^{a}ngulo de inclina\\\c{c}\\\~{a}o"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12


#set mxtics 2
#set mytics 10

#Legenda
set key top right title 'Legenda' box 5
#set key top right title "" box 5

#axis labels
set xlabel 'cos($\alpha$)'


set tics scale 2

set bars 2 #espessura das barras de erro

fit3(x)=e+f*x
fit fit3(x) 'P_Max_log_cos.txt' u 3:5 via e,f

#6
######
set xrange [0.1:1.3]
set yrange [0.5:1.3]
set ylabel 'log($P_Max$)' #rotate by  90 center 
plot 'P_Max_log_cos.txt' u 3:5:4:6 title "$P=P_Max$" w xyerrorbars ls 2,\
      fit3(x) title "Regressão linear" ls 1, \