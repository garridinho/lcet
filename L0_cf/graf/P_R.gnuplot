reset

#epslatex
set terminal epslatex size 18cm,14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'P_R.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#800000' lt 1 lw 2 pt 6 ps 2 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#ff0000' lt 1 lw 2 pt 6 ps 2
set style line 3 lc rgb '#0000ff' lt 1 lw 2 pt 6 ps 2
set style line 4 lc rgb '#ffa500' lt 1 lw 2 pt 6 ps 2
set style line 5 lc rgb '#9400d3' lt 1 lw 2 pt 6 ps 2
unset key

#Title
#set title "Pot\\\^{e}ncia do Painel"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid


#set grid back ls 12 pi -5
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12 


#set mxtics 2
#set mytics 10

#set xrange [0.6:1.9]
#set xtics 0.1,0.2,1.9
#set logscale y
#set yrange [1e7:1e22]

#Legenda
set key top right title 'Inclina\c{c}\~{a}o' box 5

#axis labels
set xlabel 'R/ $\Omega$' font "arial,20"
set ylabel 'P/ W' 
#offset 2,0

set tics scale 2

set bars 2 #espessura das barras de erro


plot 'P_R_0.txt'   u 1:2:3:4 title "$0^o$"  w xyerrorbars ls 1, \
     'P_R_15.txt'  u 1:2:3:4 title "$15^o$" w xyerrorbars ls 2, \
     'P_R_30.txt'  u 1:2:3:4 title "$30^o$" w xyerrorbars ls 3, \
     'P_R_45.txt'  u 1:2:3:4 title "$45^o$" w xyerrorbars ls 4, \
     'P_R_60.txt'  u 1:2:3:4 title "$60^o$" w xyerrorbars ls 5
