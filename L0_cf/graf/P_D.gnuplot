reset

#epslatex
set terminal epslatex size 18cm,14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'P_D.tex'


# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000f0f' lt 1 lw 2 pt 6 ps 2 #linecolour, lt,tw, pointtype,pointsize
#set style line 1 lc rgb '#ffa500' lt 1 lw 2 pt 6 ps 2 
unset key

#Title
#set title "Caracter\\\'{\\i}stica El\\\'{e}trica"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12 #insere grelha

#set mxtics 2
#set mytics 10

#set yrange [0:9]
#set ytics 1,1,9 #1st,step,last
#set logscale y
#set yrange [1e7:1e22]

#Legenda
set key top right title 'Legenda' box 5
#set key at 9.9,3.2 title 'Inclina\c{c}\~{a}o' box 5

#axis labels
set xlabel 'D/cm'
set ylabel 'P/mW' #font ",20"
set tics scale 2 

set bars 2 #espessura das barras de erro

#fits
max(x)=a+b/(x+c)/(x+c)
a=-4.7;b=15143.3;c=5.5;
fit max(x) 'P_Dcm.txt' via a,b,c

plot 'P_D.txt'   u 1:2:3:4 title "$0^o$"  w xyerrorbars ls 1, \
     max(x) title "fit P(D)" ls 1, \