reset

#epslatex
set terminal epslatex size 18cm,14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'U_I.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#800000' lt 1 lw 2 pt 6 ps 2 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#ff0000' lt 1 lw 2 pt 6 ps 2
set style line 3 lc rgb '#0000ff' lt 1 lw 2 pt 6 ps 2
set style line 4 lc rgb '#ffa500' lt 1 lw 2 pt 6 ps 2
set style line 5 lc rgb '#9400d3' lt 1 lw 2 pt 6 ps 2
unset key

#Title
#set title "Caracter\\\'{\\i}stica El\\\'{e}trica"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12 #insere grelha

#set mxtics 2
#set mytics 10

set xrange [0:9]
set xtics 1,1,9 #1st,step,last
#set logscale y
#set yrange [1e7:1e22]

#Legenda
set key top right title 'Inclina\c{c}\~{a}o' box 5
#set key at 9.9,3.2 title 'Inclina\c{c}\~{a}o' box 5

#axis labels
set xlabel 'I/ mA'
set ylabel 'U/ V' offset 2,0

set tics scale 2

set bars 2 #espessura das barras de erro


plot 'U_I_0.txt'   u 1:2:3:4 title "$0^o$"  w xyerrorbars ls 1, \
     'U_I_15.txt'  u 1:2:3:4 title "$15^o$" w xyerrorbars ls 2, \
     'U_I_30.txt'  u 1:2:3:4 title "$30^o$" w xyerrorbars ls 3, \
     'U_I_45.txt'  u 1:2:3:4 title "$45^o$" w xyerrorbars ls 4, \
     'U_I_60.txt'  u 1:2:3:4 title "$60^o$" w xyerrorbars ls 5