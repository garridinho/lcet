reset

# epslatex
set terminal epslatex size 18cm,14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'U_P_Max_cos.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#800000' lt 1 lw 2 pt 6 ps 2 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#000000' lt 1 lw 2 pt 6 ps 2


#Title
set title "Linearidade de grandezas em fun\\\c{c}\\\~{a}o do \\\^{a}ngulo de inclina\\\c{c}\\\~{a}o"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12


#set mxtics 2
#set mytics 10

#Legenda
set key top right title 'Legenda' box 5
#set key top right title "" box 5

#axis labels
set xlabel 'cos($\alpha$)'


set tics scale 2

set bars 2 #espessura das barras de erro

#3
######
set xrange [0.4:1.1]
set yrange [1.9:2.4]
set ylabel 'U/ V' #rotate by  90 center 
plot 'U_P_Max_cos.txt' u 3:5:4:6 title "$I(P=P_Max)$" w xyerrorbars ls 2,\