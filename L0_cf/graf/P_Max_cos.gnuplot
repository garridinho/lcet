reset

# epslatex
set terminal epslatex size 18cm,14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'P_Max_cos.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#800000' lt 1 lw 2 pt 6 ps 2 #linecolour, lt,tw, pointtype,pointsize
set style line 2 lc rgb '#000000' lt 1 lw 2 pt 6 ps 2

#Title
set title "Linearidade de grandezas em fun\\\c{c}\\\~{a}o do \\\^{a}ngulo de inclina\\\c{c}\\\~{a}o"

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 - tracejado
set grid back ls 12


#set mxtics 2
#set mytics 10

#Legenda
set key top right title 'Legenda' box 5
#set key top right title "" box 5

#axis labels
set xlabel 'cos($\alpha$)'


set tics scale 2

set bars 2 #espessura das barras de erro

#fit
fit2(x)=c+d*x
fit fit2(x) 'I_P_Max_cos.txt' u 3:5 via c,d

#5
######
set xrange [0.1:1.4]
set yrange [5:12]
set ylabel 'P/ mW' #rotate by  90 center 
plot 'P_Max_cos.txt' u 3:5:4:6 title "$P=P_Max$" w xyerrorbars ls 2,\
